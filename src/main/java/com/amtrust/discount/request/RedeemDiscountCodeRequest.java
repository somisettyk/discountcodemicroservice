package com.amtrust.discount.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class RedeemDiscountCodeRequest {

	@NotEmpty
	private String discountCode;

	@NotEmpty
	private String email;
}
