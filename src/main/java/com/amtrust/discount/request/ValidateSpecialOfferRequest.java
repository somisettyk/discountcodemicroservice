package com.amtrust.discount.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.time.LocalDate;

@Data
public class ValidateSpecialOfferRequest {

	@NotEmpty
	private String specialOfferName;

	@NotEmpty
	private String email;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd") 
	private LocalDate expiryDate;

}
