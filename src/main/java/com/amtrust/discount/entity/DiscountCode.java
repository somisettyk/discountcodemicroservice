package com.amtrust.discount.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "DISCOUNT_CODE")
public class DiscountCode implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String discountCode;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "RECIPIENT_ID", nullable = false)
	private Recipient recipient;

	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SPECIAL_OFFER_ID", nullable = false)
	private SpecialOffer specialOffer;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate expiryDate;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate usedDate;
	private boolean isUsed;
}
