package com.amtrust.discount.repository;

import com.amtrust.discount.entity.Recipient;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.RequestParam;

public interface RecipientRepository extends CrudRepository<Recipient, Long> {

	Recipient findByEmail(@RequestParam("email") String email);
}
