package com.amtrust.discount.repository;

import com.amtrust.discount.entity.DiscountCode;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface DiscountCodeRepository extends CrudRepository<DiscountCode, Long> {

	public List<DiscountCode> findByRecipientId(@RequestParam Long recipientId);

}
