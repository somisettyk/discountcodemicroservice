package com.amtrust.discount.repository;

import com.amtrust.discount.entity.SpecialOffer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.RequestParam;

public interface SpecialOfferRepository extends CrudRepository<SpecialOffer, Long> {

	SpecialOffer findByOfferName(@RequestParam("offerName") String offerName);
}
