package com.amtrust.discount.response;

import com.amtrust.discount.entity.SpecialOffer;
import lombok.Data;

import java.util.List;

@Data
public class SpecialOffersResponse extends Response{
	List<SpecialOffer> specialOffers;
}
