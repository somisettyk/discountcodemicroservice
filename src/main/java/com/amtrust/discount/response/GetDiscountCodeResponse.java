package com.amtrust.discount.response;

import lombok.Data;

import java.util.List;

@Data
public class GetDiscountCodeResponse extends Response {

	List<DiscountInfo> discountInfos;

	public List<DiscountInfo> getDiscountInfos() {
		return discountInfos;
	}

	public void setDiscountInfos(List<DiscountInfo> discountInfos) {
		this.discountInfos = discountInfos;
	}
}
