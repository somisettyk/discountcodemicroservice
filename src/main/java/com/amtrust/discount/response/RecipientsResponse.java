package com.amtrust.discount.response;

import com.amtrust.discount.entity.Recipient;
import lombok.Data;

import java.util.List;

@Data
public class RecipientsResponse extends Response{
	List<Recipient> recipients;
}
